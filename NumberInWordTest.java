import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NumberInWordTest {
	
	@Test
	public void teste0(){
		assertEquals("zero", NumberInWord.converteNumero(0));
	}
	@Test
	public void teste1(){
		assertEquals("um", NumberInWord.converteNumero(1));
	}
	@Test
	public void teste2(){
		assertEquals("dois", NumberInWord.converteNumero(2));
	}
	@Test
	public void teste3(){
		assertEquals("três", NumberInWord.converteNumero(3));
	}
	@Test
	public void teste4(){
		assertEquals("quatro", NumberInWord.converteNumero(4));
	}
	@Test
	public void teste5(){
		assertEquals("cinco", NumberInWord.converteNumero(5));
	}
	@Test
	public void teste6(){
		assertEquals("seis", NumberInWord.converteNumero(6));
	}
	@Test
	public void teste7(){
		assertEquals("sete", NumberInWord.converteNumero(7));
	}
	@Test
	public void teste8(){
		assertEquals("oito", NumberInWord.converteNumero(8));
	}
	@Test
	public void teste9(){
		assertEquals("nove", NumberInWord.converteNumero(9));
	}
	@Test
	public void teste10(){
		assertEquals("dez", NumberInWord.converteNumero(10));
	}
	@Test
	public void teste100(){
		assertEquals("cem", NumberInWord.converteNumero(100));
	}
	@Test
	public void teste200(){
		assertEquals("duzentos", NumberInWord.converteNumero(200));
	}
	@Test
	public void teste300(){
		assertEquals("trezentos", NumberInWord.converteNumero(300));
	}
	@Test
	public void teste400(){
		assertEquals("quatrocentos", NumberInWord.converteNumero(400));
	}
	@Test
	public void teste500(){
		assertEquals("quinhentos", NumberInWord.converteNumero(500));
	}
	@Test
	public void teste600(){
		assertEquals("seiscentos", NumberInWord.converteNumero(600));
	}
	@Test
	public void teste700(){
		assertEquals("setecentos", NumberInWord.converteNumero(700));
	}
	@Test
	public void teste800(){
		assertEquals("oitocentos", NumberInWord.converteNumero(800));
	}
	@Test
	public void teste900(){
		assertEquals("novecentos", NumberInWord.converteNumero(900));
	}
	@Test
	public void teste1000(){
		assertEquals("mil", NumberInWord.converteNumero(1000));
	}


}
