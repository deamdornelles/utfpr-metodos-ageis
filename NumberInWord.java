import java.util.Scanner;

public class NumberInWord {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Digite o número a ser convertido para palavras:");
		int numero = s.nextInt();
		
		converteNumero(numero);
		
	}
	
	public static String converteNumero(int numero) {
		
		String[] lista = new String[1001];
		String numeroString = new String();

		lista[0] = "zero";
		lista[1] = "um";
		lista[2] = "dois";
		lista[3] = "três";
		lista[4] = "quatro";
		lista[5] = "cinco";
		lista[6] = "seis";
		lista[7] = "sete";
		lista[8] = "oito";
		lista[9] = "nove";
		lista[10] = "dez";
		lista[20] = "vinte";
		lista[30] = "trinta";
		lista[40] = "quarenta";
		lista[50] = "cinquenta";
		lista[60] = "sessenta";
		lista[70] = "setenta";
		lista[80] = "oitenta";
		lista[90] = "noventa";
		lista[100] = "cem";
		lista[200] = "duzentos";
		lista[300] = "trezentos";
		lista[400] = "quatrocentos";
		lista[500] = "quinhentos";
		lista[600] = "seiscentos";
		lista[700] = "setecentos";
		lista[800] = "oitocentos";
		lista[900] = "novecentos";
		lista[1000] = "mil";
		
		for (int i = 21; i < 30 ;i++) {
			lista[i] = lista[20] + " e " + lista[i - 20];
		}
		
		for (int i = 31; i < 40 ;i++) {
			lista[i] = lista[30] + " e " + lista[i - 30];
		}
		
		for (int i = 41; i < 50 ;i++) {
			lista[i] = lista[40] + " e " + lista[i - 40];
		}
		
		for (int i = 51; i < 60 ;i++) {
			lista[i] = lista[50] + " e " + lista[i - 50];
		}
		
		for (int i = 61; i < 70 ;i++) {
			lista[i] = lista[60] + " e " + lista[i - 60];
		}
		
		for (int i = 71; i < 80 ;i++) {
			lista[i] = lista[70] + " e " + lista[i - 70];
		}
		
		for (int i = 81; i < 90 ;i++) {
			lista[i] = lista[80] + " e " + lista[i - 80];
		}
		
		for (int i = 91; i < 100 ;i++) {
			lista[i] = lista[90] + " e " + lista[i - 90];
		}
		
		for (int i = 0; i < lista.length; i++) {
			if (lista[i] != null) {
				if (numero == i) {
					System.out.println("Número convertido: " + lista[i]);
					numeroString = lista[i];					
				}
			}
		}
		
		return numeroString;
		
	}

}
